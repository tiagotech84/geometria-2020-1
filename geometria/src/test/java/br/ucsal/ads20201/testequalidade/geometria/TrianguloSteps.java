package br.ucsal.ads20201.testequalidade.geometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class TrianguloSteps {
	
	private static WebDriver driver;

	@BeforeAll
	public static void setupChrome(){
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		driver = new ChromeDriver();
	}
	/*
	@BeforeAll
	public static void setupFirefox(){
		System.setProperty("webdriver.chrome.driver","./driver/geckodriver.exe");
		driver = new FirefoxDriver();
	}*/
	@Test
	public void testar() throws InterruptedException {
		// Abrir página do Investing.com
		driver.get("src/main/resource/webapp/triangulo.html");
	}
	
	@Given("seleciono o tipo de cálculo $tipoCalculo")
	public void tipoCalculo(String tipoCalculo) {
		WebElement selecionarCalculo = driver.findElement(By.id("tipoCalculoSelect"));
		selecionarCalculo.sendKeys(tipoCalculo + Keys.ENTER);
	}

	@When("informo $valor para $local")
	public void valorLocal(String valor, String local) {
		WebElement definirValorLocal = driver.findElement(By.id(local));
		definirValorLocal.sendKeys(valor, Keys.ENTER);

	}


	@Then("calculada será $situacaoEsperada")
	public void verificarCalculo(String situacaoEsperada) throws InterruptedException {
		Thread.sleep(5000);
		String contem = driver.getPageSource();
		Assert.assertTrue(contem.contains(situacaoEsperada));
	
	}
	@AfterAll
	public static void teardown(){
		driver.quit();
	}


}
